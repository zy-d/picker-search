// components/picker-search/picker-search.js
Component({
    /**
     * 组件的属性列表
     */
    options: {
        addGlobalClass: true
    },
    properties: {
        innerText: {
            type: String,
            value: 'default value',
        },
        pickerData: {
            type: Array,
            value: [],
            observer: "watchPickerData"
        },
        pickerValue: {
            type: Number,
            value: 0,
            observer: 'watchPickerValue'
        },
        valueKey: {
            type: String,
            value: 'name'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        defalutValue: [0],
        show: false,
        _pickerData: [],
        backupData: [],
        fieldName: '',
        Vmodel: '',
        itemData: {} // 确定后选中某一项的数据
    },
    // 监听变化
    observers: {
        
    },
    // 生命周期函数
    lifetimes: {
        attached: function() {
            // 在组件实例进入页面节点树时执行
        },
        detached: function() {
            // 在组件实例被从页面节点树移除时执行
        },
    },
    /**
     * 组件的方法列表
     */
    methods: {
        // 监听列表数据
        watchPickerData(newVal) {
            
            let arr = newVal;
            if (newVal && newVal.length > 0) {
                this.setData({
                    _pickerData: arr,
                    backupData: arr, // 备份数据，用于搜索
                    itemData: {
                        data: arr[this.data.defalutValue], // 初始化下标第几项数据
                        pickerIndex: [0]
                    },
                    defalutValue: [this.properties.pickerValue], // 选中那一项数据
                    fieldName: arr[this.data.defalutValue][this.data.valueKey] // 获取选中的名字
                })
            }
        },
        // 监听选项
        watchPickerValue(newVal) {
            this.setData({
                defalutValue: [newVal], // 选中那一项数据
                fieldName: this.data._pickerData.length > 0 ? this.data._pickerData[newVal][this.data.valueKey] : '' // 获取选中的名字
            })
        },
        // 封装搜索功能
        pickerInput(e) {
            var _this = this
            let val = this.data.Vmodel; // 要用简易双向绑定，不要直接获取 e.detail.value 这样很多bug；
            
            if (val && val.trim !== '') {
                let _pickerData = _this.data.backupData.filter(item => item[this.properties.valueKey].indexOf(val)>-1);
                
                var index = this.data.itemData['pickerIndex'][0];

                if (_pickerData.length < (index+1)) {
                    index = _pickerData.length - 1 > 0? _pickerData.length -1:0;
                    
                }
                if (_pickerData.length > 0) {
                    // 筛选出来的数据重新赋值给pickerData
                    this.setData({
                        _pickerData: _pickerData,
                        itemData: {  // 这数据要返回给父组件
                            pickerIndex: [index], // 获取滚动某一项的下标
                            data: this.data._pickerData[index]
                        }
                    })
                } else {
                    this.setData({
                        _pickerData: _pickerData,
                        itemData: {  // 这数据要返回给父组件
                            pickerIndex: [0], // 获取滚动某一项的下标
                        }
                    })
                }

                
            } else {
      
                this.setData({
                    _pickerData: this.data.backupData,
                    itemData: {  // 这数据要返回给父组件
                        pickerIndex: this.data.itemData['pickerIndex'], // 获取滚动某一项的下标
                        data: this.data.backupData[this.data.itemData['pickerIndex']]
                    }
                })
            }
        },

        // 显示
        bindShowPicker () {
            this.setData({
                show: true
            })
        },

        // 滚动事件
        pickerChange (e) {
            // pickerValue
            this.setData({
                itemData: {  // 这数据要返回给父组件
                    pickerIndex: e.detail.value, // 获取滚动某一项的下标
                    data: this.data._pickerData[e.detail.value]
                }
            }, () => {
                this.triggerEvent('pickerChange', this.data.itemData)
            })
        },
        pickerStart () {
            this.triggerEvent('pickerStart')
        },
        pickerEnd() {
            this.triggerEvent('pickerEnd')
        },
        // 点击确认
        pickerComfirm() {
            let selData = this.data._pickerData[this.data.itemData.pickerIndex];
            this.setData({
                fieldName: selData[this.properties.valueKey],
                defalutValue: [this.data.itemData['pickerIndex']] // 确认后把选中的下标赋值给 defalutValue
            }, () => {
                this.setData({show: false}, () => {
                    this.triggerEvent('pickerComfirm', selData)
                })
            })
        },
        // 取消
        pickerCancle () {
            this.setData({show: false}, () => {
                this.triggerEvent('pickerCancle')
            })
        }
    }
})

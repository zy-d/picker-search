const app = getApp()

Page({
  data: {
    pickerValue: 0,
    pickerData: [
        {
            areaName: '虚拟test',
            id: 1
        },
        {
            areaName: '虚拟2',
            id: 2
        },
        {
            areaName: '虚拟4',
            id: 3
        },
        {
            areaName: '虚拟7',
            id: 4
        },
        {
            areaName: '虚拟223',
            id: 5
        },
        {
            areaName: '虚拟213',
            id: 6
        }
    ]
  },
  onLoad: function () {
    console.log('代码片段是一种迷你、可分享的小程序或小游戏项目，可用于分享小程序和小游戏的开发经验、展示组件和 API 的使用、复现开发问题和 Bug 等。可点击以下链接查看代码片段的详细文档：')
    console.log('https://mp.weixin.qq.com/debug/wxadoc/dev/devtools/devtools.html')
  },

  pickerComfirm: function (e) {
      console.log('diaoyong:----', e.detail)
  }
})
